var bootState = {
    preload: function () {
    // Load the progress bar image.
    game.load.image('bar', 'assets/bar.png');
    game.load.spritesheet('player','assets/P1/out_par.png',50,50);
    game.load.spritesheet('player2','assets/P2/out_par.png',50,50);
    game.load.spritesheet('player3','assets/P3/out_par.png',50,50);
    game.load.spritesheet('player4','assets/P4/out_par.png',50,50);
    },

    create: function() {
    // Set some game settings.
    //game.stage.backgroundColor = '#000000';
    game.physics.startSystem(Phaser.Physics.ARCADE);
    game.renderer.renderSession.roundPixels = true;
    // Start the load state.
    game.state.start('load');
    }
};