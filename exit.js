var exitState = {
	preload:function(){
	},
	create:function(){
		//---------------------------------about text
		this.background_color="#66ff99";
		this.font_color="#fff";
		game.stage.backgroundColor=this.background_color;
		game.add.tileSprite(0, 0, 1280, 640, 'endbg');

		this.title_style={font:'50px Arial',fill:"tomato",fontWeight:'bold'};

		this.title_game=game.add.text(game.width/2 - 200,game.height/8*2 + 20,'WINNIER TEAM',this.title_style);
		this.title_game.anchor.setTo(0.5,0.5);

		if(game.gamemode==1){
			if(game.team1score>game.team2score){
				this.create_player1_img(game.width/2 - 60 ,game.height/8*3 + 30);
				this.create_player2_img(game.width/2 - 340 ,game.height/8*3 + 30);
				this.imgmode = 1;
				this.winner=game.add.text(game.width/2 - 200,game.height/8*3 + 30, 'Player', this.title_style);
				this.winner.anchor.setTo(0.5,0.5);
			}
			else{
				this.create_player3_img(game.width/2 - 60 ,game.height/8*3 + 30);
				this.create_player4_img(game.width/2 - 340 ,game.height/8*3 + 30);
				this.imgmode = 2;
				this.winner=game.add.text(game.width/2 - 200,game.height/8*3 + 30, 'AI', this.title_style);
				this.winner.anchor.setTo(0.5,0.5);
			}
		}
		else{
			if(game.team1score>game.team2score){
				this.create_player1_img(game.width/2 - 60 ,game.height/8*3 + 30);
				this.create_player3_img(game.width/2 - 340 ,game.height/8*3 + 30);
				this.imgmode = 3;
				this.winner=game.add.text(game.width/2 - 200,game.height/8*3 + 30, 'Team1', this.title_style);
				this.winner.anchor.setTo(0.5,0.5);
			}
			else{
				this.create_player2_img(game.width/2 - 60 ,game.height/8*3 + 30);
				this.create_player4_img(game.width/2 - 340 ,game.height/8*3 + 30);
				this.winner=game.add.text(game.width/2 - 200,game.height/8*3 + 30, 'Team2', this.title_style);
				this.winner.anchor.setTo(0.5,0.5);
				this.imgmode = 4;
			}
		}

		this.font_style={font:'30px Arial',fill:this.font_color};
		this.font_style2={font:'30px Arial',fill:'steelgray'};
		this.font_style_picked={font:'30px Arial',fill:"yellow",fontWeight:'bold'};
		
		this.score1=game.add.text(game.width/2 + 200 ,game.height/8*2 + 60,'player1 score : ' + player1_score.toString(),this.font_style2);
		this.score1.anchor.setTo(0.5,0.5);
		this.score2=game.add.text(game.width/2 + 200 ,game.height/8*3 + 50,'player2 score : ' + player2_score.toString(),this.font_style2);
		this.score2.anchor.setTo(0.5,0.5);

		this.playAgain=game.add.text(game.width/2,420,'Play again',this.font_style);
		this.playAgain.anchor.setTo(0.5,0.5);
		this.toMenu=game.add.text(game.width/2,480,'Go back to Menu',this.font_style);
		this.toMenu.anchor.setTo(0.5,0.5);
		
		this.lea_game=game.add.text(game.width/2,540,'Quit',this.font_style);
		this.lea_game.anchor.setTo(0.5,0.5);
		
		this.menu_obj=[this.playAgain,this.toMenu,this.lea_game];
	},
	create_player1_img:function(x,y){
		this.player1_i=game.add.sprite(x,y,'player');
		this.player1_i.anchor.setTo(0.5,0.5);
		this.player1_i.animations.add('down',[12,13,14,15],10);
	},
	create_player2_img:function(x,y){
		this.player2_i=game.add.sprite(x,y,'player2');
		this.player2_i.anchor.setTo(0.5,0.5);
		this.player2_i.animations.add('down',[12,13,14,15],10);
	},
	create_player3_img:function(x,y){
		this.player3_i=game.add.sprite(x,y,'player3');
		this.player3_i.anchor.setTo(0.5,0.5);
		this.player3_i.animations.add('down',[12,13,14,15],10);
	},
	create_player4_img:function(x,y){
		this.player4_i=game.add.sprite(x,y,'player4');
		this.player4_i.anchor.setTo(0.5,0.5);
		this.player4_i.animations.add('down',[12,13,14,15],10);
	},

	update:function(){
		if(this.imgmode==1){
			this.player2_i.animations.play('down');
			this.player1_i.animations.play('down');
		}
		else if(this.imgmode==2){
			this.player3_i.animations.play('down');
			this.player4_i.animations.play('down');
		}
		else if(this.imgmode==3){
			this.player3_i.animations.play('down');
			this.player1_i.animations.play('down');
		}
		else {
			this.player2_i.animations.play('down');
			this.player4_i.animations.play('down');
		}
		this.check();
	},
	check:function(){
		var x=game.input.mousePointer.x;
		var y=game.input.mousePointer.y;
		var press=game.input.activePointer.leftButton.isDown;
		var m=this.menu_obj;
		for(var i=0;i<m.length;++i){
			if(x>m[i].x-m[i].width/2&&x<m[i].x+m[i].width/2&&y>m[i].y-m[i].height/2&&y<m[i].y+m[i].height/2){
				m[i].setStyle(this.font_style_picked);
				if(press){
					if(i==0)this.start();
					else if(i==1)this.goToMenu();
					else this.leave();
				};
			}
			else{
				m[i].setStyle(this.font_style);
			}
		}
	},
    start:function(){
		if(game.gamemode==1)game.state.start('play');
		else game.state.start('play2');
    },
	leave:function(){
		var random = Math.floor((Math.random() * 100) + 1);
		if(random >= 1 && random < 25)
			window.location.href="https://105062218.gitlab.io/Assignment_02";
		else if(random >= 25 && random < 50)
			window.location.href="https://105062220.gitlab.io/Assignment_02";
		else if(random >= 50 && random < 75)
			window.location.href="https://105062227.gitlab.io/Assignment_02";
		else
			window.location.href="https://105062230.gitlab.io/Assignment_02";
		
	},
	goToMenu:function() {
		game.state.start('menu');
	}
};