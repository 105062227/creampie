var game = new Phaser.Game(1280, 640, Phaser.AUTO, 'canvas');
var player1_score = 0;
var player2_score = 0;
// Define our global variable
game.global = { score: 0 , health: 10 , name: "", best: 0, leaderBoard: [], win: 'player'};

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('rule', ruleState);
game.state.add('control', controlState);
game.state.add('props_i', props_iState);
game.state.add('leader', leaderState);
game.state.add('play', playState);
game.state.add('play2', play2State);
game.state.add('exit', exitState);
game.state.start('boot');