var loadState = {
    preload: function () {

    // Display the progress bar
    var bar = game.add.sprite(240, 280, 'bar');
    //bar.anchor.setTo(0.5, 0.5);
    game.load.setPreloadSprite(bar);
    var player = game.add.sprite(400, 200, 'player');
    player.anchor.setTo(0.5, 0.5);
    player.scale.setTo(2, 2);
    player.frame = 4;
    player.animations.add('right',[4,5,6,7],8,true);
    player.animations.play('right');
    var player2 = game.add.sprite(560, 200, 'player2');
    player2.anchor.setTo(0.5, 0.5);
    player2.scale.setTo(2, 2);
    player2.frame = 4;
    player2.animations.add('right',[4,5,6,7],8,true);
    player2.animations.play('right');
    var player3 = game.add.sprite(720, 200, 'player3');
    player3.anchor.setTo(0.5, 0.5);
    player3.scale.setTo(2, 2);
    player3.frame = 4;
    player3.animations.add('right',[4,5,6,7],8,true);
    player3.animations.play('right');
    var player4 = game.add.sprite(880, 200, 'player4');
    player4.anchor.setTo(0.5, 0.5);
    player4.scale.setTo(2, 2);
    player4.frame = 4;
    player4.animations.add('right',[4,5,6,7],8,true);
    player4.animations.play('right');

    // Load all game assets
    game.load.spritesheet('player','assets/P1/out_par.png',50,50);
	game.load.spritesheet('player2','assets/P2/out_par.png',50,50);
	game.load.spritesheet('player3','assets/P3/out_par.png',50,50);
	game.load.spritesheet('player4','assets/P4/out_par.png',50,50);
	
    game.load.image('menubg', 'assets/menubg.png');
    game.load.image('rule', 'assets/rule.png');
    game.load.image('endbg', 'assets/endbg.png');
    game.load.image('props_i', 'assets/props_i.png');
    game.load.image('control', 'assets/control.png');
    game.load.image('infobg', 'assets/infobg.jpg');
	game.load.image('shield','assets/shield.png',70,70);

    game.load.spritesheet('bomb', 'assets/water_bomb.png',50,50);
	game.load.image('water', 'assets/water.png');
	game.load.image('blood', 'assets/blood.png');
    game.load.image('energy', 'assets/energy.png');
    game.load.image('wall2', 'assets/wall2.png');
    game.load.image('treeGreen_large', 'assets/treeGreen_large.png');
    game.load.image('b01', 'assets/b01.png');
    game.load.image('b03', 'assets/b03.png');
    game.load.image('b04', 'assets/b04.png');
    game.load.image('b06', 'assets/b06.png');
    game.load.image('b07', 'assets/b07.png');
    game.load.image('ball', 'assets/ball.png');
    game.load.image('glove', 'assets/glove.png');
    game.load.image('shoe', 'assets/shoe.png');
    game.load.image('shield', 'assets/shield.png');
    game.load.image('heart', 'assets/heart.png');
    game.load.image('tileset', 'assets/tileset.png');
    game.load.tilemap('map', 'assets/map.json', null,Phaser.Tilemap.TILED_JSON);
    game.load.tilemap('map2', 'assets/map2.json', null,Phaser.Tilemap.TILED_JSON);
    game.load.audio('buttonMusic', 'assets/button.wav');
    game.load.audio('hitMusic', 'assets/hit.wav');
    game.load.audio('menubgmMusic', 'assets/menubgm.ogg');
    game.load.audio('playbgmMusic', 'assets/playbgm.ogg');

    // Load a new asset that we will use in the menu state
    //game.load.image('background', 'assets/background.png');
    },

    create: function() {
    // Go to the menu state
    game.time.events.add(Phaser.Timer.SECOND*2, function(){game.state.start('menu');}, this);
    }
}; 