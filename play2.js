var play2State = {
	create: function() {
		// Removed background color, physics system, and roundPixels.
		// replace 'var score = 0' by global score variable.
		game.global.score = 0;
		//game.global.health = 10;
		//infos
		this.hitMusic = game.add.audio('hitMusic');
    	this.hitMusic.loop = false;
		this.playbgmMusic = game.add.audio('playbgmMusic');
    	this.playbgmMusic.loop = true;
    	this.playbgmMusic.play();
		game.add.tileSprite(896, 0, 384, 640, 'infobg');
		//set tile map
		this.map = game.add.tilemap(game.map_name);
        this.map.addTilesetImage('tileset');
		this.map.addTilesetImage('wall2');
		this.map.addTilesetImage('treeGreen_large');
		this.map.addTilesetImage('b01');
		this.map.addTilesetImage('b03');
		this.map.addTilesetImage('b04');
		this.map.addTilesetImage('b06');
		this.map.addTilesetImage('b07');
		this.map.addTilesetImage('ball');
		this.map.addTilesetImage('glove');
		this.map.addTilesetImage('shoe');
		this.map.addTilesetImage('shield');
		this.map.addTilesetImage('heart');
        this.layer1 = this.map.createLayer('Layer1');
		this.layer2 = this.map.createLayer('Layer2');
		this.layer3 = this.map.createLayer('Layer3');
		this.layer4 = this.map.createLayer('Layer4');
		this.physics.arcade.TILE_BIAS = 50;
        this.layer1.resizeWorld();
		this.layer2.resizeWorld();
		this.layer3.resizeWorld();
		this.map.setCollision([41,50],true,this.layer2);
		this.map.setCollision([42,43,44],true,this.layer3);
		this.map.setCollision([45,46,47,48,49],true,this.layer4);
		
		// The following part is the same as in previous lecture.

		this.cursor = game.input.keyboard.createCursorKeys();
		this.shiftKey = game.input.keyboard.addKey(Phaser.Keyboard.SHIFT);
		this.shiftKey2 = game.input.keyboard.addKey(Phaser.Keyboard.G);
		this.wasd={
			up: game.input.keyboard.addKey(Phaser.Keyboard.W),
			down: game.input.keyboard.addKey(Phaser.Keyboard.S),
			left: game.input.keyboard.addKey(Phaser.Keyboard.A),
			right: game.input.keyboard.addKey(Phaser.Keyboard.D),
		};

		//set up player
		//move to create_player function
		this.create_player();
		this.create_shield();
		this.create_bomb();
		
		this.text_thr_edge_y=320;
		this.text_fou_edge_y=475;
		this.bar_thr_edge_y=320;
		this.bar_fou_edge_y=475;
		
		this.edge_x=280;
		this.interval_y=32;
		
		this.text_size=22;
		this.text_edge_x=this.edge_x;
		this.text_edge_y=37;
		this.text_interval_y=this.interval_y;
		
		this.bar_edge_x=180;
		this.bar_edge_y=65;
		this.bar_interval_y=this.interval_y;
		this.bar_interval=15;
		this.bar_scale_x=3;
		this.bar_scale_y=4;
		
		this.create_text();
		this.create_bar();
		this.create_player_img();
		
		this.dash = false;
		this.dashEnergy = 0;
		this.isDashing = false;
		this.dash2 = false;
		this.dashEnergy2 = 0;
		this.isDashing2 = false;
		this.lastDirKey1 = 'right';
		this.lastDirKey2 = 'right';
		this.checkLastKey();
    },
	create_player:function(){
		this.max_water=6;
		this.min_water=3;
		this.max_speed=400;
		this.min_speed=250;
		this.heart_blood=20;
		
		this.player1 = game.add.sprite(100, 100, 'player');
		this.player1.animations.add('left',[0,1,2,3],10);
		this.player1.animations.add('right',[4,5,6,7],10);
		this.player1.animations.add('up',[8,9,10,11],10);
		this.player1.animations.add('down',[12,13,14,15],10);
		this.player1.frame = 12;
		this.player1.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player1);
		this.player1.body.setSize(25, 33, 11, 8); //hitbox
		this.player1face = 'right';
		this.player1dash = 0;
		this.player1.score=0;
		this.player1.blood=100;
		this.player1.energy=0;
		this.player1.dashDamage = false;  //ㄧ次衝刺只造成ㄧ次傷害
		this.player1.max_water=4;
		this.player1.water=4;
		this.player1.push=false;
		this.player1.shield=false;
		this.player1.shoe=false;
		this.player1.speed=250;
		
		this.player2 = game.add.sprite(95, 540, 'player2');
		this.player2.animations.add('left',[0,1,2,3],10);
		this.player2.animations.add('right',[4,5,6,7],10);
		this.player2.animations.add('up',[8,9,10,11],10);
		this.player2.animations.add('down',[12,13,14,15],10);
		this.player2.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player2);
		this.player2.body.setSize(25, 33, 11, 8); //hitbox
		this.player2face = 'right';
		this.player2.score=0;
		this.player2.blood=100;
		this.player2.energy=0;
		this.player2.dashDamage = false  //ㄧ次衝刺只造成ㄧ次傷害
		this.player2.max_water=4;
		this.player2.water=4;
		this.player2.push=false;
		this.player2.shield=false;
		this.player2.shoe=false;
		this.player2.speed=250;
		
		this.player3 = game.add.sprite(350, 480, 'player3');
		this.player3.animations.add('left',[0,1,2,3],10);
		this.player3.animations.add('right',[4,5,6,7],10);
		this.player3.animations.add('up',[8,9,10,11],10);
		this.player3.animations.add('down',[12,13,14,15],10);
		this.player3.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player3);
		this.player3face = 'right';
		this.player3.score=0;
		this.player3.blood=100;
		this.player3.energy=0;
		this.player3.dashDamage = false  //ㄧ次衝刺只造成ㄧ次傷害
		this.player3.max_water=4;
		this.player3.water=4;
		this.player3.push=false;
		this.player3.shield=false;
		this.player3.shoe=false;
		this.player3.speed=150;
		
		this.player3.dir=0;
		this.player3.buffer=-1;
		this.player3.prev=-1;
		this.player3.trace=false;
		this.player3.mutex=true;
		this.player3.collide=false;
		this.player3.set_bomb_mutex=true;
		this.player3.dash_cd = false;
		this.player3.dash_energy = 0;
		this.player3.dash_energy_goal = 0;
		this.player3.isDashing = false;
		this.player3.dashDamage = false;
		
		this.player4 = game.add.sprite(660, 200, 'player4');
		this.player4.animations.add('left',[0,1,2,3],10);
		this.player4.animations.add('right',[4,5,6,7],10);
		this.player4.animations.add('up',[8,9,10,11],10);
		this.player4.animations.add('down',[12,13,14,15],10);
		this.player4.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(this.player4);
		this.player4face = 'right';
		this.player4.score=0;
		this.player4.blood=100;
		this.player4.energy=0;
		this.player4.dashDamage = false  //ㄧ次衝刺只造成ㄧ次傷害
		this.player4.max_water=4;
		this.player4.water=4;
		this.player4.push=false;
		this.player4.shield=false;
		this.player4.shoe=false;
		this.player4.speed=150;
		
		this.player4.dir=2;
		this.player4.buffer=-1;
		this.player4.prev=-1;
		this.player4.trace=false;
		this.player4.mutex=true;
		this.player4.collide=false;
		this.player4.set_bomb_mutex=true;
		this.player4.dash_cd = false;
		this.player4.dash_energy = 0;
		this.player4.dash_energy_goal = 0;
		this.player4.isDashing = false;
		this.player4.dashDamage = false;
		
		this.players=[this.player1,this.player2,this.player3,this.player4];
		this.ai_players=[this.player3,this.player4];

		game.team1score = 0;
		game.team2score = 0;
	},
	create_shield:function(){
		this.shield1=game.add.sprite(this.player1.x,this.player1.y,'shield');
		this.shield1.alpha=0;
		this.shield1.anchor.setTo(0.5,0.5);
		this.shield2=game.add.sprite(this.player2.x,this.player2.y,'shield');
		this.shield2.alpha=0;
		this.shield2.anchor.setTo(0.5,0.5);
		
		this.shield3=game.add.sprite(this.player3.x,this.player3.y,'shield');
		this.shield3.alpha=0;
		this.shield3.anchor.setTo(0.5,0.5);
		this.shield4=game.add.sprite(this.player4.x,this.player4.y,'shield');
		this.shield4.alpha=0;
		this.shield4.anchor.setTo(0.5,0.5);
		this.shields=[this.shield1,this.shield2,this.shield3,this.shield4];
	},
	create_bomb:function(){
		this.bs = game.add.group();
		this.bs.enableBody = true;
		this.bs.setAll('body.immovable', true);
		
		this.bs_ori = game.add.group();
		this.bs_ori.enableBody = true;
		this.bs_ori.setAll('body.immovable',true);
	},
	create_text_score:function(){
		this.score_color="#0033cc";
		this.score_style={font:this.text_size.toString()+'px Arial',fill:this.score_color};
		this.score_edge_x=this.text_edge_x;
		this.score_edge_y=this.text_edge_y;
		this.score_label_one = game.add.text(game.width-this.score_edge_x-77,this.score_edge_y+2,'', this.score_style);
		this.score_label_one.setText('Player 1 : '+this.player1.score.toString());
		this.score_label_one.anchor.setTo(0,0.5);
		this.score_label_two = game.add.text(game.width-this.score_edge_x-77,155+this.score_edge_y,'', this.score_style);
		this.score_label_two.setText('Player 3 : '+this.player3.score.toString());
		this.score_label_two.anchor.setTo(0,0.5);
		
		this.score_label_thr = game.add.text(game.width-this.score_edge_x-77,this.text_thr_edge_y+this.score_edge_y,'', this.score_style);
		this.score_label_thr.setText('Player 2 : '+this.player2.score.toString());
		this.score_label_thr.anchor.setTo(0,0.5);
		this.score_label_fou = game.add.text(game.width-this.score_edge_x-77,this.text_fou_edge_y+this.score_edge_y,'', this.score_style);
		this.score_label_fou.setText('Player 4 : '+this.player4.score.toString());
		this.score_label_fou.anchor.setTo(0,0.5);
	},
	create_text_blood:function(){
		this.blood_color="#ff0066";
		this.blood_style={font:this.text_size.toString()+'px Arial',fill:this.blood_color};
		this.blood_edge_x=this.text_edge_x;
		this.blood_edge_y=this.text_edge_y+this.text_interval_y;
		this.blood_label_one = game.add.text(game.width-this.blood_edge_x,this.blood_edge_y,'', this.blood_style);
		this.blood_label_one.setText('Blood');
		this.blood_label_one.anchor.setTo(0,0.5);
		this.blood_label_two = game.add.text(game.width-this.blood_edge_x,155+this.blood_edge_y,'', this.blood_style);
		this.blood_label_two.setText('Blood');
		this.blood_label_two.anchor.setTo(0,0.5);
		
		this.blood_label_thr = game.add.text(game.width-this.blood_edge_x,this.text_thr_edge_y+this.blood_edge_y,'', this.blood_style);
		this.blood_label_thr.setText('Blood');
		this.blood_label_thr.anchor.setTo(0,0.5);
		this.blood_label_fou = game.add.text(game.width-this.blood_edge_x,this.text_fou_edge_y+this.blood_edge_y,'', this.blood_style);
		this.blood_label_fou.setText('Blood');
		this.blood_label_fou.anchor.setTo(0,0.5);
	},
	create_text_energy:function(){
		this.energy_color="orange";
		this.energy_style={font:this.text_size.toString()+'px Arial',fill:this.energy_color};
		this.energy_edge_x=this.text_edge_x;
		this.energy_edge_y=this.text_edge_y+2*this.text_interval_y;
		this.energy_label_one = game.add.text(game.width-this.energy_edge_x,this.energy_edge_y,'', this.energy_style);
		this.energy_label_one.setText('Energy');
		this.energy_label_one.anchor.setTo(0,0.5);
		this.energy_label_two = game.add.text(game.width-this.energy_edge_x,155+this.energy_edge_y,'', this.energy_style);
		this.energy_label_two.setText('Energy');
		this.energy_label_two.anchor.setTo(0,0.5);
		
		this.energy_label_thr = game.add.text(game.width-this.energy_edge_x,this.text_thr_edge_y+this.energy_edge_y,'', this.energy_style);
		this.energy_label_thr.setText('Energy');
		this.energy_label_thr.anchor.setTo(0,0.5);
		this.energy_label_fou = game.add.text(game.width-this.energy_edge_x,this.text_fou_edge_y+this.energy_edge_y,'', this.energy_style);
		this.energy_label_fou.setText('Energy');
		this.energy_label_fou.anchor.setTo(0,0.5);
	},
	create_text_water:function(){
		this.water_color="#0099ff";
		this.water_style={font:this.text_size.toString()+'px Arial',fill:this.water_color};
		this.water_edge_x=this.text_edge_x;
		this.water_edge_y=this.text_edge_y+3*this.text_interval_y;
		this.water_label_one = game.add.text(game.width-this.water_edge_x,this.water_edge_y,'', this.water_style);
		this.water_label_one.setText('Bomb');
		this.water_label_one.anchor.setTo(0,0.5);
		this.water_label_two = game.add.text(game.width-this.water_edge_x,155+this.water_edge_y,'', this.water_style);
		this.water_label_two.setText('Bomb');
		this.water_label_two.anchor.setTo(0,0.5);
		
		this.water_label_thr = game.add.text(game.width-this.water_edge_x,this.text_thr_edge_y+this.water_edge_y,'', this.water_style);
		this.water_label_thr.setText('Bomb');
		this.water_label_thr.anchor.setTo(0,0.5);		
		this.water_label_fou = game.add.text(game.width-this.water_edge_x,this.text_fou_edge_y+this.water_edge_y,'', this.water_style);
		this.water_label_fou.setText('Bomb');
		this.water_label_fou.anchor.setTo(0,0.5);
	},
	create_text:function(){
		this.create_text_score();
		this.create_text_blood();
		this.create_text_energy();
		this.create_text_water();
	},
	create_bar_blood:function(){
		this.blood_bar_edge_x=this.bar_edge_x;
		this.blood_bar_edge_y=this.bar_edge_y;
		this.blood_bar_interval=this.bar_interval;
		var tmp=[];
		for(var i=0;i<10;++i){
			tmp[i]=game.add.sprite(game.width-this.blood_bar_edge_x+i*this.blood_bar_interval,this.blood_bar_edge_y,'blood');
			tmp[i].anchor.setTo(0,0.5);
			tmp[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
		}
		this.player1.blood_bar=tmp;
		var temp=[];
		for(var i=0;i<10;++i){
			temp[i]=game.add.sprite(game.width-this.blood_bar_edge_x+i*this.blood_bar_interval,this.bar_thr_edge_y+this.blood_bar_edge_y,'blood');
			temp[i].anchor.setTo(0,0.5);
			temp[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
		}
		this.player2.blood_bar=temp;
		var kkk=[];
		for(var i=0;i<10;++i){
			kkk[i]=game.add.sprite(game.width-this.blood_bar_edge_x+i*this.blood_bar_interval,155+this.blood_bar_edge_y,'blood');
			kkk[i].anchor.setTo(0,0.5);
			kkk[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
		}
		this.player3.blood_bar=kkk;
		var nig=[];
		for(var i=0;i<10;++i){
			nig[i]=game.add.sprite(game.width-this.blood_bar_edge_x+i*this.blood_bar_interval,this.bar_fou_edge_y+this.blood_bar_edge_y,'blood');
			nig[i].anchor.setTo(0,0.5);
			nig[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
		}
		this.player4.blood_bar=nig;
	},
	create_bar_energy:function(){
		this.energy_bar_edge_x=this.bar_edge_x;
		this.energy_bar_edge_y=this.bar_edge_y+this.bar_interval_y;
		this.energy_bar_interval=this.bar_interval;
		var tmp=[];
		for(var i=0;i<10;++i){
			tmp[i]=game.add.sprite(game.width-this.energy_bar_edge_x+i*this.energy_bar_interval,this.energy_bar_edge_y,'energy');
			tmp[i].anchor.setTo(0,0.5);
			tmp[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
			tmp[i].alpha=0;
		}
		this.player1.energy_bar=tmp;
		var temp=[];
		for(var i=0;i<10;++i){
			temp[i]=game.add.sprite(game.width-this.energy_bar_edge_x+i*this.energy_bar_interval,this.bar_thr_edge_y+this.energy_bar_edge_y,'energy');
			temp[i].anchor.setTo(0,0.5);
			temp[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
			temp[i].alpha=0;
		}
		this.player2.energy_bar=temp;
		
		var kkk=[];
		for(var i=0;i<10;++i){
			kkk[i]=game.add.sprite(game.width-this.energy_bar_edge_x+i*this.energy_bar_interval,155+this.energy_bar_edge_y,'energy');
			kkk[i].anchor.setTo(0,0.5);
			kkk[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
			kkk[i].alpha=0;
		}
		this.player3.energy_bar=kkk;
		var nig=[];
		for(var i=0;i<10;++i){
			nig[i]=game.add.sprite(game.width-this.energy_bar_edge_x+i*this.energy_bar_interval,this.bar_fou_edge_y+this.energy_bar_edge_y,'energy');
			nig[i].anchor.setTo(0,0.5);
			nig[i].scale.setTo(this.bar_scale_x,this.bar_scale_y);
			nig[i].alpha=0;
		}
		this.player4.energy_bar=nig;
	},
	create_bar_water:function(){
		this.water_bar_edge_x=this.bar_edge_x+10;
		this.water_bar_edge_y=this.bar_edge_y+2*this.bar_interval_y;
		this.water_bar_interval=23;
		var tmp=[];
		for(var i=0;i<6;++i){
			tmp[i]=game.add.sprite(game.width-this.water_bar_edge_x+i*this.water_bar_interval,this.water_bar_edge_y,'bomb');
			tmp[i].anchor.setTo(0,0.5);
			tmp[i].scale.setTo(0.7,0.7);
			if(i>3)tmp[i].alpha=0;
		}
		this.player1.water_bar=tmp;
		var temp=[];
		for(var i=0;i<6;++i){
			temp[i]=game.add.sprite(game.width-this.water_bar_edge_x+i*this.water_bar_interval,this.bar_thr_edge_y+this.water_bar_edge_y,'bomb');
			temp[i].anchor.setTo(0,0.5);
			temp[i].scale.setTo(0.7,0.7);
			if(i>3)temp[i].alpha=0;
		}
		this.player2.water_bar=temp;
		
		var kkk=[];
		for(var i=0;i<6;++i){
			kkk[i]=game.add.sprite(game.width-this.water_bar_edge_x+i*this.water_bar_interval,155+this.water_bar_edge_y,'bomb');
			kkk[i].anchor.setTo(0,0.5);
			kkk[i].scale.setTo(0.7,0.7);
			if(i>3)kkk[i].alpha=0;
		}
		this.player3.water_bar=kkk;
		
		var nig=[];
		for(var i=0;i<6;++i){
			nig[i]=game.add.sprite(game.width-this.water_bar_edge_x+i*this.water_bar_interval,this.bar_fou_edge_y+this.water_bar_edge_y,'bomb');
			nig[i].anchor.setTo(0,0.5);
			nig[i].scale.setTo(0.7,0.7);
			if(i>3)nig[i].alpha=0;
		}
		this.player4.water_bar=nig;
	},
	create_bar:function(){
		this.create_bar_blood();
		this.create_bar_energy();
		this.create_bar_water();
	},
	create_player_img:function(){
		this.player1_i=game.add.sprite(game.width-this.score_edge_x-43,100,'player');
		this.player1_i.anchor.setTo(0.5,0.5);
		this.player1_i.animations.add('down',[12,13,14,15],10);
		
		this.player2_i=game.add.sprite(game.width-this.score_edge_x-43,420,'player2');
		this.player2_i.anchor.setTo(0.5,0.5);
		this.player2_i.animations.add('down',[12,13,14,15],10);
		
		this.player3_i=game.add.sprite(game.width-this.score_edge_x-43,255,'player3');
		this.player3_i.anchor.setTo(0.5,0.5);
		this.player3_i.animations.add('down',[12,13,14,15],10);
		
		this.player4_i=game.add.sprite(game.width-this.score_edge_x-43,575,'player4');
		this.player4_i.anchor.setTo(0.5,0.5);
		this.player4_i.animations.add('down',[12,13,14,15],10);
		
	},
	//------------------------------------------
	update: function() {
		//console.log(this.dashEnergy);
		//console.log(this.dashEnergy2);
		this.check_collide();
		this.check_dash();
		
		this.movePlayer();
		this.player4_i.animations.play('down');
		this.player3_i.animations.play('down');
		this.player2_i.animations.play('down');
		this.player1_i.animations.play('down');
		
		for(var t=0; t<4; t++){
			if(this.players[t].blood<=0){
				this.players[t].kill();
				this.players[t].die = true;
			}
			else this.players[t].die = false;
		}
        //if(!this.player1.inWorld) this.playerDie();
        //if(game.global.health<=0) this.playerDie();
		
        var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        //spaceKey.onDown.add(this.setBomb1, this);
		spaceKey.onDown.add(this.set_bomb,null,0,0,this.players,this.bs,this.bs_ori);
        var QKey = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        //QKey.onDown.add(this.setBomb2, this);
		QKey.onDown.add(this.set_bomb,null,0,1,this.players,this.bs,this.bs_ori);
		this.refresh_score();
		this.refresh_bar();
		this.refresh_shield();
		this.control_ai_player();

		this.check_game_over();
    },
	control_ai_player:function(){
		this.control_ai_trace(2);
		this.control_ai_trace(3);
		this.control_ai_dir(2);
		this.control_ai_dir(3);
		this.move_ai(2);
		this.move_ai(3);
		this.set_bomb_ai(2);
		this.set_bomb_ai(3);
	},
	set_bomb_ai:function(n){
		if(this.players[n].die)return;
		var player=this.players[n];
		var enemy1 = this.players[n==2?1:0];
		var enemy2 = this.players[n==2?3:2];
		if(player.set_bomb_mutex&& (this.distance([player.x,player.y],[enemy1.x,enemy1.y])<150 || this.distance([player.x,player.y],[enemy2.x,enemy2.y])<150)){
			this.set_bomb(0,n,this.players,this.bs,this.bs_ori);
			player.set_bomb_mutex=false;
			setTimeout(()=>{
				player.set_bomb_mutex=true;
			},Math.random()*5000);
		}
	},
	control_ai_trace:function(n){
		var player=this.players[n];
		if(player.mutex){
			player.mutex=false;
			if(player.trace==false){
				setTimeout(()=>{
					player.trace=true;
					setTimeout(()=>{
						player.trace=false;
						player.mutex=true;
					},(Math.random()<0.4?Math.random()*1000:Math.random()*2000));
				},(Math.random()<0.4?Math.random()*1000:Math.random()*2000));
			}
		}
	},
	control_ai_dir:function(n){
		var player=this.players[n];
		var enemy=this.players[n==2?1:0];
		
		var tmp,temp;
		if(player.dash_cd == true){
		if(player.trace){
			tmp=player.buffer;
			temp=this.cal_collide_direction(enemy,player);
			if(player.collide){
				var r=Math.random();
				var t=[];
				var k=0;
				for(var i=0;i<3;++i){
					if(i!=tmp&&i!=player.prev)t[k++]=i;
				}
				if(r<1/2)tmp=t[0];
				else tmp=t[1];
				player.dir=tmp;
				player.prev=tmp;
			}
			player.dir=temp;
		}
		else{
			tmp=player.buffer;
			if(player.collide){
				var r=Math.random();
				var t=[];
				var k=0;
				for(var i=0;i<3;++i){
					if(i!=tmp&&i!=player.prev)t[k++]=i;
					}
				if(r<1/2)tmp=t[0];
				else tmp=t[1];
				player.dir=tmp;
				player.prev=tmp;
			}
		}
		}
	},
	distance:function(a,b){
		return Math.sqrt(Math.pow(Math.abs(a[0]-b[0]),2)+Math.pow(Math.abs(a[1]-b[1]),2));
	},
	move_ai:function(n){
		var player=this.players[n];
		if(player.die)return;
		if(player.dash_cd == false && player.dash_energy_goal == 0){
			player.dash_energy_goal = Math.floor((Math.random() * 100) + 50);
		}
		if(player.dash_energy < player.dash_energy_goal){
			player.dash_energy++;
			player.body.velocity.x = 0;
			player.body.velocity.y = 0;
			this.emitter=game.add.emitter(player.x, player.y+25, 1);
			this.emitter.makeParticles('energy');
			this.emitter.gravity=0;
			this.emitter.setXSpeed(-150, 150);
			this.emitter.setYSpeed(-150, 0);
			this.emitter.setScale(2,0,2,0,800);
			this.emitter.start(true,1000,null,50);
		}
		else if(player.dash_energy == player.dash_energy_goal){
			player.dash_energy++;
			player.isDashing = true;
			setTimeout(function(){
				player.dash_cd = false;
				player.dash_energy = 0;
				player.dash_energy_goal =0;
			}.bind(this), 5000);
			if(player.dir == 0){
				player.body.velocity.x = -2500;
				setTimeout(function(){player.body.velocity.x = 0; player.dash_cd = true; player.isDashing = false;}.bind(this),player.dash_energy*2);
			}
			else if(player.dir == 1){
				player.body.velocity.x = 2500;
				setTimeout(function(){player.body.velocity.x = 0; player.dash_cd = true; player.isDashing = false;}.bind(this),player.dash_energy*2);
			}
			else if(player.dir == 2){
				player.body.velocity.y = -2500;
				setTimeout(function(){player.body.velocity.y = 0; player.dash_cd = true; player.isDashing = false;}.bind(this),player.dash_energy*2);
			}
			else{
				player.body.velocity.y = 2500;
				setTimeout(function(){player.body.velocity.y = 0; player.dash_cd = true; player.isDashing = false;}.bind(this),player.dash_energy*2);
			}
		}
		if(player.dir==0 && player.dash_cd == true){
			player.body.velocity.x=-1*player.speed;
			player.body.velocity.y=0;
			player.animations.play('left');
		}
		else if(player.dir==1 && player.dash_cd == true){
			player.body.velocity.x=player.speed;
			player.body.velocity.y=0;
			player.animations.play('right');
		}
		else if(player.dir==2 && player.dash_cd == true){
			player.body.velocity.x=0;
			player.body.velocity.y=-1*player.speed;
			player.animations.play('up');
		}
		else if(player.dash_cd == true){
			player.body.velocity.x=0;
			player.body.velocity.y=player.speed;
			player.animations.play('down');
		}
	},
	check_game_over:function(){
		if((this.player1.blood<=0&&this.player2.blood<=0)||(this.player3.blood<=0&&this.player1.blood<=0)||(this.player2.blood<=0&&this.player4.blood<=0)){
			setTimeout(()=>{
				this.game_over();
			},1000);
		}
	},
	game_over:function(){
		game.team1score=this.player1.score+this.player3.score;
		game.team2score=this.player2.score+this.player4.score;
		player1_score = this.player1.score;
		player2_score = this.player2.score;
		this.playbgmMusic.stop();
		game.state.start('exit');
	},
	check_dash:function(){
		if(Phaser.Math.distance(this.player1.body.x,this.player1.body.y,this.player2.body.x,this.player2.body.y) < 50){
			if(this.isDashing == true && this.player1.dashDamage == false){
				this.player2.blood -= 40;
				this.hitMusic.play();
				this.player1.dashDamage = true;
			}
			if(this.isDashing2 == true && this.player2.dashDamage == false){
				this.player1.blood -= 40;
				this.hitMusic.play();
				this.player2.dashDamage = true;
			}
		}
		if(Phaser.Math.distance(this.player1.body.x,this.player1.body.y,this.player4.body.x,this.player4.body.y) < 50){
			if(this.isDashing == true && this.player1.dashDamage == false){
				this.player4.blood -= 40;
				this.hitMusic.play();
				this.player1.dashDamage = true;
			}
			if(this.player4.isDashing == true && this.player4.dashDamage == false){
				this.player1.blood -= 40;
				this.hitMusic.play();
				this.player4.dashDamage = true;
			}
		}
		if(Phaser.Math.distance(this.player2.body.x,this.player2.body.y,this.player3.body.x,this.player3.body.y) < 50){
			if(this.isDashing2 == true && this.player2.dashDamage == false){
				this.player3.blood -= 40;
				this.hitMusic.play();
				this.player2.dashDamage = true;
			}
			if(this.player3.isDashing == true && this.player3.dashDamage == false){
				this.player2.blood -= 40;
				this.hitMusic.play();
				this.player3.dashDamage = true;
			}
		}
		if(Phaser.Math.distance(this.player3.body.x,this.player3.body.y,this.player4.body.x,this.player4.body.y) < 50){
			if(this.player3.isDashing == true && this.player3.dashDamage == false){
				this.player4.blood -= 40;
				this.hitMusic.play();
				this.player3.dashDamage = true;
			}
			if(this.player4.isDashing == true && this.player4.dashDamage == false){
				this.player3.blood -= 40;
				this.hitMusic.play();
				this.player4.dashDamage = true;
			}
		}
		if(this.isDashing == false){
			this.player1.dashDamage = false;
		}
		if(this.isDashing2 == false){
			this.player2.dashDamage = false;
		}
		if(this.player3.isDashing == false){
			this.player3.dashDamage = false;
		}
		if(this.player4.isDashing == false){
			this.player4.dashDamage = false;
		}
	},
	check_collide:function(){
		game.physics.arcade.collide(this.player1, this.layer2);
		game.physics.arcade.collide(this.player2, this.layer2);
		game.physics.arcade.collide(this.player1, this.layer3);
		game.physics.arcade.collide(this.player2, this.layer3);
		
		this.player3.buffer=-1;
		this.player4.buffer=-1;
		this.player3.collide=false;
		this.player4.collide=false;
		game.physics.arcade.collide(this.player3, this.layer2,(player,tile)=>{
			player.buffer=this.cal_collide_direction(tile,player);
			player.collide=true;
		},null,this);
		game.physics.arcade.collide(this.player4, this.layer2,(player,tile)=>{
			player.buffer=this.cal_collide_direction(tile,player);
			player.collide=true;
		},null,this);
		game.physics.arcade.collide(this.player3, this.layer3,(player,tile)=>{
			player.buffer=this.cal_collide_direction(tile,player);
			player.collide=true;
		},null,this);
		game.physics.arcade.collide(this.player4, this.layer3,(player,tile)=>{
			player.buffer=this.cal_collide_direction(tile,player);
			player.collide=true;
		},null,this);
		
		game.physics.arcade.collide(this.bs_ori,this.layer2);
		game.physics.arcade.collide(this.bs_ori,this.layer3);
		
		//tile collide
        game.physics.arcade.collide(this.bs, this.layer2, (bomb)=>{
			bomb.kill();
			this.add_score(bomb.num,30);
		}, null, this);
		game.physics.arcade.collide(this.bs, this.layer3, (bomb,tile)=>{
			bomb.kill();
			var n = Math.floor(Math.random() * (5));
			this.map.putTile(45 + n , tile.x, tile.y,this.layer4);
			this.map.removeTile(tile.x, tile.y, this.layer3).destroy();
		}, null, this);

        game.physics.arcade.collide(this.bs,this.player1, (player,bomb)=>{
			this.player_collide_bomb(0,bomb);
			if(bomb.num==1||bomb.num==3)this.add_score(bomb.num,100);
		},null,this);
		game.physics.arcade.collide(this.bs,this.player2, (player,bomb)=>{
			this.player_collide_bomb(1,bomb);
			if(bomb.num==0||bomb.num==2)this.add_score(bomb.num,100);
		},null,this);
		game.physics.arcade.collide(this.bs,this.player3, (player,bomb)=>{
			this.player_collide_bomb(2,bomb);
			if(bomb.num==1||bomb.num==3)this.add_score(bomb.num,100);
		},null,this);
		game.physics.arcade.collide(this.bs,this.player4, (player,bomb)=>{
			this.player_collide_bomb(3,bomb);
			if(bomb.num==0||bomb.num==2)this.add_score(bomb.num,100);
		},null,this);


		this.push_bomb(0);
		this.push_bomb(1);
		this.push_bomb(2);
		this.push_bomb(3);
		
		//吃道具
		game.physics.arcade.collide(this.layer4,this.player1, (player,tile)=>{
			this.pick_thing(0,tile);
			this.map.removeTile(tile.x, tile.y, this.layer4).destroy();
		},null,this);
		game.physics.arcade.collide(this.layer4,this.player2, (player,tile)=>{
			this.pick_thing(1,tile);
			this.map.removeTile(tile.x, tile.y, this.layer4).destroy();
		},null,this);
		game.physics.arcade.collide(this.layer4,this.player3, (player,tile)=>{
			this.pick_thing(2,tile);
			this.map.removeTile(tile.x, tile.y, this.layer4).destroy();
		},null,this);
		game.physics.arcade.collide(this.layer4,this.player4, (player,tile)=>{
			this.pick_thing(3,tile);
			this.map.removeTile(tile.x, tile.y, this.layer4).destroy();
		},null,this);
	},
	push_bomb:function(n){
		var player=this.players[n];
		if(player.push){
			game.physics.arcade.collide(this.bs_ori,player);
		}
	},
	player_collide_bomb:function(n,bomb){
		var player=this.players[n];
		bomb.kill();
		if(!player.shield){
			this.hitMusic.play();
			player.blood-=20;
			player.score-=40;
			this.create_blood(player.x,player.y,this.cal_collide_direction(bomb,player));
		}
		else{
			this.create_water(player.x,player.y,this.cal_collide_direction(bomb,player));
		}
		//if(player.max_water>this.min_water)--player.max_water;
	},
	pick_thing:function(n,tile){
		//console.log(tile.index);
		var player=this.players[n];
		player.score+=50;
		if(tile.index==48){
			if(player.max_water<this.max_water){
				++player.max_water;
				++player.water;
			}
		}
		else if(tile.index==47&&player.push==false){
			player.push=true;
			setTimeout(()=>{
				player.push=false;
			},5000);
		}
		else if(tile.index==46&&player.shield==false){
			player.shield=true;
			this.take_shield(n);
			setTimeout(()=>{
				this.untake_shield(n);
				player.shield=false;
			},5000);
		}
		else if(tile.index==45&&player.shoe==false){
			player.shoe=true;
			this.take_shoe(n);
			setTimeout(()=>{
				this.untake_shoe(n);
				player.shoe=false;
			},5000);
		}
		else if(tile.index==49){
			console.log(player.blood);
			player.blood+=this.heart_blood;
			if(player.blood>=100)player.blood=100;
			console.log(player.blood);
		}
	},
	thing_effect:function(n){
		this.refresh_shield(n);
	},
	refresh_shield:function(){
		for(var i=0;i<4;++i){
			this.move_shield(i);
		}
	},
	move_shield:function(n){
		this.shields[n].x=this.players[n].x;
		this.shields[n].y=this.players[n].y;
	},
	take_shield:function(n){
		this.shields[n].alpha=0.3;
	},
	untake_shield:function(n){
		this.shields[n].alpha=0;
	},
	take_shoe:function(n){
		this.players[n].speed=this.max_speed;
	},
	untake_shoe:function(n){
		this.players[n].speed=this.min_speed;
	},
	cal_collide_direction:function(one,two){
		return Math.abs(one.x-two.x)>Math.abs(one.y-two.y)?(one.x<two.x?0:1):(one.y<two.y?2:3)
	},
	refresh_bar:function(){
		this.equal(0);
		this.equal(1);
		for(var i=0;i<4;++i){
			this.refresh_blood_bar(i);
			this.refresh_energy_bar(i);
			this.refresh_water_bar(i);
		}
	},
	equal:function(n){
		if(n==0)this.players[n].energy=this.dashEnergy;
		else if(n==1)this.players[n].energy=this.dashEnergy2;
	},
	refresh_blood_bar:function(n){
		var blood=this.players[n].blood-1;
		if(blood<0)blood=-1;
		blood=Math.floor(blood/10);
		for(var i=0;i<10;i++){
			this.players[n].blood_bar[i].alpha=1;
		}
		for(var i=blood+1;i<10;++i){
			this.players[n].blood_bar[i].alpha=0;
		}
	},
	refresh_energy_bar:function(n){
		var energy=this.players[n].energy;
		if(energy>=100)energy=100;
		energy=Math.floor(energy/10);
		var i;
		for(i=0;i<energy;++i){
			this.players[n].energy_bar[i].alpha=1;
		}
		for(var j=i;j<10;++j){
			this.players[n].energy_bar[j].alpha=0;
		}
	},
	refresh_water_bar:function(n){
		var player=this.players[n];
		var water=player.water;
		for(var i=0;i<this.max_water;++i){
			player.water_bar[i].alpha=0;
		}
		for(var i=0;i<player.water;++i){
			player.water_bar[i].alpha=1;
		}
	},
	add_score:function(n,score){
		this.players[n].score+=score;
	},
	refresh_score:function(){
		this.score_label_one.setText('Player 1 : '+this.player1.score.toString());
		this.score_label_two.setText('Player 3 : '+this.player3.score.toString());
		this.score_label_thr.setText('Player 2 : '+this.player2.score.toString());
		this.score_label_fou.setText('Player 4 : '+this.player4.score.toString());
	},
	create_water:function(x,y,n){
		//n=0 bomb comes from left
		//n=1 bomb comes from right
		//n=2 bomb comes from top
		//n=3 bomb comes from down
		//console.log(n);
		var r=[Math.random(),Math.random()];
		var main_speed=150;
		var other_speed=100;
		if(n==0)dir=[main_speed,main_speed+100,-(other_speed*r[0]),other_speed*r[1]];
		else if(n==1)dir=[-1*(main_speed+100),-1*main_speed,-(other_speed*r[0]),other_speed*r[1]];
		else if(n==2)dir=[-(other_speed*r[0]),other_speed*r[1],main_speed,main_speed+100];
		else dir=[-(other_speed*r[0]),other_speed*r[1],-1*(main_speed+100),-1*main_speed];
		this.emitter=game.add.emitter(x,y,50);
		this.emitter.makeParticles('water');
		this.emitter.gravity=0;
		this.emitter.setXSpeed(dir[0], dir[1]);
		this.emitter.setYSpeed(dir[2], dir[3]);
		this.emitter.setScale(3,0,3,0,800);
		this.emitter.start(true,1000,null,50);
	},
	create_blood:function(x,y,n){
		//console.log(n);
		var r=[1,1];
		var main_speed=100;
		var other_speed=50;
		if(n==0)dir=[main_speed,main_speed+100,-1*(other_speed*r[0]),other_speed*r[1]];
		else if(n==1)dir=[-1*(main_speed+100),-1*main_speed,-1*(other_speed*r[0]),other_speed*r[1]];
		else if(n==2)dir=[-1*(other_speed*r[0]),other_speed*r[1],main_speed,main_speed+100];
		else dir=[-1*(other_speed*r[0]),other_speed*r[1],-1*(main_speed+100),-1*main_speed];
		this.emitter=game.add.emitter(x,y,20);
		this.emitter.makeParticles('blood');
		this.emitter.gravity=0;
		this.emitter.setXSpeed(dir[0], dir[1]);
		this.emitter.setYSpeed(dir[2], dir[3]);
		this.emitter.setScale(2,0,2,0,1000);
		this.emitter.start(true,1000,null,20);
	},
	set_bomb:function(a,n,players,bs,bs_ori){
		var player=players[n];
		if(player.water==0||player.die)return;
		else --player.water;
		var bomb=game.add.sprite(player.x,player.y,'bomb',0,bs_ori);
		bomb.anchor.setTo(0.5,0.5);
		game.physics.arcade.enable(bomb);
		bomb.num=n;
		bomb.speed=800;
		bomb.body.bounce.set(1);
		game.time.events.add(Phaser.Timer.SECOND*3,()=>{
			var dir=[[1,0],[0,1],[-1,0],[0,-1]];
			for(var i=0;i<4;++i){
				var ex_bomb=game.add.sprite(bomb.x,bomb.y,'bomb',0,bs);
				ex_bomb.anchor.setTo(0.5,0.5);
				game.physics.arcade.enable(ex_bomb);
				ex_bomb.alpha=1;
				ex_bomb.body.velocity.x=dir[i][0]*bomb.speed;
				ex_bomb.body.velocity.y=dir[i][1]*bomb.speed;
				ex_bomb.num=bomb.num;
			}
			bomb.kill();
			if(player.water<player.max_water)++player.water;
		},this);
		
		//game.time.events.add(Phaser.Timer.SECOND*4,function(){console.log(1),this});
	},

    movePlayer: function() {
		if(!this.player1.die&&this.shiftKey.isDown && (this.cursor.left.isDown || this.cursor.right.isDown || this.cursor.up.isDown || this.cursor.down.isDown))
        {
            this.dash = true;
			if(this.dashEnergy < 100)
				this.dashEnergy ++;
			this.player1.body.velocity.x = 0;
            this.player1.body.velocity.y = 0;
			this.emitter=game.add.emitter(this.player1.x, this.player1.y+25, 1);
			this.emitter.makeParticles('energy');
			this.emitter.gravity=0;
			this.emitter.setXSpeed(-150, 150);
			this.emitter.setYSpeed(-150, 0);
			this.emitter.setScale(2,0,2,0,800);
			this.emitter.start(true,1000,null,50);
        }
        else if(this.shiftKey.isUp && this.dash == true){
			this.isDashing = true;
			if(this.player1face == 'left'){
				this.player1.body.velocity.x = -2500;
				setTimeout(function(){this.player1.body.velocity.x = 0; this.isDashing = false}.bind(this),this.dashEnergy*2);
			}
			else if(this.player1face == 'right'){
				this.player1.body.velocity.x = 2500;
				setTimeout(function(){this.player1.body.velocity.x = 0; this.isDashing = false}.bind(this),this.dashEnergy*2);
			}
			else if(this.player1face == 'up'){
				this.player1.body.velocity.y = -2500;
				setTimeout(function(){this.player1.body.velocity.y = 0; this.isDashing = false}.bind(this),this.dashEnergy*2);
			}
			else if(this.player1face == 'down'){
				this.player1.body.velocity.y = 2500;
				setTimeout(function(){this.player1.body.velocity.y = 0; this.isDashing = false}.bind(this),this.dashEnergy*2);
			}
			this.dash = false;
			this.dashEnergy = 0;
        }
		
        else if (this.cursor.left.isDown && this.isDashing == false && this.lastDirKey1 == 'left'){
            this.player1face = 'left';
            this.player1.body.velocity.x = -1*this.player1.speed;
            this.player1.body.velocity.y = 0;
			this.player1.animations.play('left');
        }
        else if (this.cursor.right.isDown && this.isDashing == false && this.lastDirKey1 == 'right'){
            this.player1face = 'right';
            this.player1.body.velocity.x = this.player1.speed;
            this.player1.body.velocity.y = 0;
			this.player1.animations.play('right');
        }// If neither the right or left arrow key is pressed
        else if (this.cursor.up.isDown && this.isDashing == false && this.lastDirKey1 == 'up'){
            this.player1face = 'up';
            this.player1.body.velocity.y = -1*this.player1.speed;
            this.player1.body.velocity.x = 0;
			this.player1.animations.play('up');
        }
        else if (this.cursor.down.isDown && this.isDashing == false && this.lastDirKey1 == 'down'){
            this.player1face = 'down';
            this.player1.body.velocity.y = this.player1.speed;
            this.player1.body.velocity.x = 0;
			this.player1.animations.play('down');
        }
		else if(this.isDashing == false){
			this.player1.body.velocity.x = 0;
            this.player1.body.velocity.y = 0;
			if(this.player1face == 'left')
				this.player1.frame = 0;
			else if(this.player1face == 'up')
				this.player1.frame = 8;
			else if(this.player1face == 'right')
				this.player1.frame = 4;
			else 
				this.player1.frame = 12;
		}
		
		if(!this.player2.die&&this.shiftKey2.isDown && (this.wasd.left.isDown || this.wasd.right.isDown || this.wasd.up.isDown || this.wasd.down.isDown))
        {
            this.dash2 = true;
			if(this.dashEnergy2 < 100)
				this.dashEnergy2 ++;
			this.player2.body.velocity.x = 0;
            this.player2.body.velocity.y = 0;
			this.emitter=game.add.emitter(this.player2.x, this.player2.y+25, 1);
			this.emitter.makeParticles('energy');
			this.emitter.gravity=0;
			this.emitter.setXSpeed(-150, 150);
			this.emitter.setYSpeed(-150, 0);
			this.emitter.setScale(2,0,2,0,800);
			this.emitter.start(true,1000,null,50);
        }
        else if(this.shiftKey2.isUp && this.dash2 == true){
			this.isDashing2 = true;
			if(this.player2face == 'left'){
				this.player2.body.velocity.x = -2500;
				setTimeout(function(){this.player2.body.velocity.x = 0; this.isDashing2 = false}.bind(this),this.dashEnergy2*4);
			}
			else if(this.player2face == 'right'){
				this.player2.body.velocity.x = 2500;
				setTimeout(function(){this.player2.body.velocity.x = 0; this.isDashing2 = false}.bind(this),this.dashEnergy2*4);
			}
			else if(this.player2face == 'up'){
				this.player2.body.velocity.y = -2500;
				setTimeout(function(){this.player2.body.velocity.y = 0; this.isDashing2 = false}.bind(this),this.dashEnergy2*4);
			}
			else if(this.player2face == 'down'){
				this.player2.body.velocity.y = 2500;
				setTimeout(function(){this.player2.body.velocity.y = 0; this.isDashing2 = false}.bind(this),this.dashEnergy2*4);
			}
			this.dash2 = false;
			this.dashEnergy2 = 0;
        }
        else if (this.wasd.left.isDown && this.isDashing2 == false && this.lastDirKey2 == 'left'){
            this.player2face = 'left';
            this.player2.body.velocity.x = -1*this.player2.speed;
            this.player2.body.velocity.y = 0;
			this.player2.animations.play('left');
        }
        else if (this.wasd.right.isDown && this.isDashing2 == false && this.lastDirKey2 == 'right'){
            this.player2face = 'right';
            this.player2.body.velocity.x = this.player2.speed;
            this.player2.body.velocity.y = 0;
			this.player2.animations.play('right');
        }// If neither the right or left arrow key is pressed
        else if (this.wasd.up.isDown && this.isDashing2 == false && this.lastDirKey2 == 'up'){
            this.player2face = 'up';
            this.player2.body.velocity.y = -1*this.player2.speed;
            this.player2.body.velocity.x = 0;
			this.player2.animations.play('up');
        }
        else if (this.wasd.down.isDown && this.isDashing2 == false && this.lastDirKey2 == 'down'){
            this.player2face = 'down';
            this.player2.body.velocity.y = this.player2.speed;
            this.player2.body.velocity.x = 0;
			this.player2.animations.play('down');
        }
        else if(this.isDashing2 == false){ 
        // Stop the player
            this.player2.body.velocity.x = 0;
            this.player2.body.velocity.y = 0;
			if(this.player2face == 'left')
				this.player2.frame = 0;
			else if(this.player2face == 'up')
				this.player2.frame = 8;
			else if(this.player2face == 'right')
				this.player2.frame = 4;
			else 
				this.player2.frame = 12;
        }


    },
	
	checkLastKey: function() {
		this.cursor.left.onDown.add(function (){
			this.lastDirKey1 = 'left';
		},this);
		this.cursor.right.onDown.add(function (){
			this.lastDirKey1 = 'right';
		},this);
		this.cursor.up.onDown.add(function (){
			this.lastDirKey1 = 'up';
		},this);
		this.cursor.down.onDown.add(function (){
			this.lastDirKey1 = 'down';
		},this);
		this.wasd.left.onDown.add(function (){
			this.lastDirKey2 = 'left';
		},this);
		this.wasd.right.onDown.add(function (){
			this.lastDirKey2 = 'right';
		},this);
		this.wasd.up.onDown.add(function (){
			this.lastDirKey2 = 'up';
		},this);
		this.wasd.down.onDown.add(function (){
			this.lastDirKey2 = 'down';
		},this);
	}
};